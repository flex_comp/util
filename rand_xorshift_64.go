package util

import (
	"fmt"
	"github.com/lazybeaver/xorshift"
)

func NewRandXOrShift64(seed int64) *XOrShift64 {
	r := new(XOrShift64)
	r.Seed(seed)
	return r
}

type XOrShift64 struct {
	raw xorshift.XorShift
}

func (x *XOrShift64) Seed(seed int64) {
	x.raw = xorshift.NewXorShift64Star(uint64(seed))
}

func (x *XOrShift64) Int63() int64 {
	return int64(x.Uint64() & (1<<63 - 1))
}

func (x *XOrShift64) Uint64() uint64 {
	return x.raw.Next()
}

func (x *XOrShift64) UintRange(min, max uint64) uint64 {
	n := x.raw.Next()
	fmt.Println(n)
	return n%(max-min) + min
}

func (x *XOrShift64) UintRangeInclusive(min, max uint64) uint64 {
	return x.raw.Next()%(max-min+1) + min
}
