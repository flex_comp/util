package util

import (
	"github.com/fsnotify/fsnotify"
)

type FolderWatcherEvent struct {
	Create chan string
	Modify chan string
	Delete chan string
	Rename chan string
}

func newFolderWatcherEvent() *FolderWatcherEvent {
	return &FolderWatcherEvent{
		Create: make(chan string, 1),
		Modify: make(chan string, 1),
		Delete: make(chan string, 1),
		Rename: make(chan string, 1),
	}
}

func FolderWatcher(dir string) (*FolderWatcherEvent, error) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return nil, err
	}

	ev := newFolderWatcherEvent()
	go func() {
		defer watcher.Close()

		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}

				if event.Op&fsnotify.Create == fsnotify.Create {
					ev.Create <- event.Name
				}

				if event.Op&fsnotify.Write == fsnotify.Write {
					ev.Modify <- event.Name
				}

				if event.Op&fsnotify.Remove == fsnotify.Remove {
					ev.Delete <- event.Name
				}

				if event.Op&fsnotify.Rename == fsnotify.Rename {
					ev.Rename <- event.Name
				}
			case _, ok := <-watcher.Errors:
				if !ok {
					return
				}
			}
		}
	}()

	err = watcher.Add(dir)
	if err != nil {
		return nil, err
	}

	return ev, nil
}
