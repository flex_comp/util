package util

import (
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
)

func LoadFile(path string) ([]byte, error) {
	srcFile, e := os.OpenFile(path, os.O_RDWR, 0666)
	if e != nil {
		return nil, e
	}

	defer srcFile.Close()
	raw, e := ioutil.ReadAll(srcFile)
	if e != nil {
		return nil, e
	}

	return raw, nil
}

func LoadFromDir(dir, suffix string) (map[string]string, error) {
	m := make(map[string]string)

	err := filepath.Walk(dir,
		func(p string, i os.FileInfo, e error) error {
			if e != nil {
				return e
			}

			if i.IsDir() {
				return nil
			}

			ext := path.Ext(i.Name())
			if strings.ReplaceAll(ext, ".", "") != suffix {
				return nil
			}

			raw, e := LoadFile(p)
			if e != nil {
				return nil
			}

			name := path.Base(i.Name())
			idx := strings.LastIndex(name, ".")
			if idx >= 0 {
				name = name[:idx]
			}

			m[name] = string(raw)
			return nil
		})

	return m, err
}
