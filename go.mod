module gitlab.com/flex_comp/util

go 1.16

require (
	github.com/fsnotify/fsnotify v1.5.1
	github.com/json-iterator/go v1.1.11
	github.com/lazybeaver/xorshift v0.0.0-20170702203709-ce511d4823dd
	github.com/spf13/cast v1.4.0
	github.com/tyler-smith/go-bip32 v1.0.0
	github.com/tyler-smith/go-bip39 v1.1.0
)
