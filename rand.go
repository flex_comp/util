package util

import (
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func RandInt(min, max int, s ...*rand.Rand) int {
	if len(s) == 0 {
		return rand.Intn(max-min) + min
	}

	return s[0].Intn(max-min) + min
}

func RandIntInclusive(min, max int, s ...*rand.Rand) int {
	if len(s) == 0 {
		return rand.Intn(max-min+1) + min
	}

	return s[0].Intn(max-min+1) + min
}

func RandInt32(min, max int32, s ...*rand.Rand) int32 {
	if len(s) == 0 {
		return rand.Int31n(max-min) + min
	}

	return s[0].Int31n(max-min) + min
}

func RandInt32Inclusive(min, max int32, s ...*rand.Rand) int32 {
	if len(s) == 0 {
		return rand.Int31n(max-min+1) + min
	}

	return s[0].Int31n(max-min+1) + min
}

func RandInt64(min, max int64, s ...*rand.Rand) int64 {
	if len(s) == 0 {
		return rand.Int63n(max-min) + min
	}

	return s[0].Int63n(max-min) + min
}

func RandInt64Inclusive(min, max int64, s ...*rand.Rand) int64 {
	if len(s) == 0 {
		return rand.Int63n(max-min+1) + min
	}

	return s[0].Int63n(max-min+1) + min
}
