package util

type randWeightItem struct {
	begin int
	end   int
	value interface{}
}

type RandWeight struct {
	random *XOrShift64
	data   []*randWeightItem
	size   int
}

func NewRandWeight(r *XOrShift64) *RandWeight {
	return &RandWeight{
		random: r,
		data:   make([]*randWeightItem, 0),
		size:   0,
	}
}

func (r *RandWeight) Add(weight int, value interface{}) {
	if weight <= 0 {
		return
	}

	r.data = append(r.data, &randWeightItem{
		begin: r.size,
		end:   r.size + weight,
		value: value,
	})

	r.size += weight
}

func (r *RandWeight) Random() interface{} {
	if r.size == 0 {
		return nil
	}

	random := r.random.UintRange(0, uint64(r.size)) //RandInt64(0, int64(r.size), r.random)
	for _, item := range r.data {
		if int(random) < item.end {
			return item.value
		}
	}

	return nil
}
