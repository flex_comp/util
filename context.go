package util

type ContextCE struct {
	Custom      map[string]interface{}
	LocalCustom map[string]interface{}
	Config      map[string]interface{}
	External    interface{}
	Args        []interface{}
}
