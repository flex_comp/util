package util

import "strings"

func ParseIPByAddr(addr string) string {
	ipIdx := strings.LastIndex(addr, ":")
	if ipIdx < 0 {
		return ""
	}

	return addr[:ipIdx]
}

