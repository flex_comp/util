package util

import (
	"fmt"
	"testing"
	"time"
)

func TestFolderWatcher(t *testing.T) {
	t.Run("folder_watcher", func(t *testing.T) {
		got, err := FolderWatcher("./test/watcher")
		if err != nil {
			t.Failed()
		}

		go func() {
			for {
				select {
				case dir := <-got.Create:
					fmt.Println("create:", dir)
				case dir := <-got.Modify:
					fmt.Println("modify:", dir)
				case dir := <-got.Rename:
					fmt.Println("rename:", dir)
				case dir := <-got.Delete:
					fmt.Println("delete:", dir)
				}
			}
		}()
		<-time.After(time.Hour)
	})
}
