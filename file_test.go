package util

import (
	"testing"
)

func TestLoadFromDir(t *testing.T) {
	t.Run("load_from_dir", func(t *testing.T) {
		m, e := LoadFromDir("./test", "")
		if e != nil {
			t.Fatal(e)
		}

		for f, _ := range m {
			t.Log(f)
		}
	})
}
