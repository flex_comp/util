package util

import jsoniter "github.com/json-iterator/go"

var (
	json jsoniter.API
)

func init() {
	json = jsoniter.Config{
		EscapeHTML:                    false,
		ObjectFieldMustBeSimpleString: true, // do not unescape object field
		CaseSensitive:                 true,
	}.Froze()
}

func JSONUnmarshal(data []byte, v interface{}) error {
	return json.Unmarshal(data, v)
}

func JSONMarshal(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}
