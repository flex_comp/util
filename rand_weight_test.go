package util

import (
	"fmt"
	"testing"
)

func TestNewRandWeight(t *testing.T) {
	t.Run("rand weight", func(t *testing.T) {
		rw := NewRandWeight(NewRandXOrShift64(13))
		rw.Add(10, 1)
		rw.Add(1, 2)
		fmt.Println(rw.Random())
	})
}
