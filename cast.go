package util

import (
	"github.com/spf13/cast"
	"time"
)

func ToTimeE(i interface{}) (tim time.Time, err error) {
	return cast.ToTimeE(i)
}

func ToTimeInDefaultLocationE(i interface{}, location *time.Location) (tim time.Time, err error) {
	return cast.ToTimeInDefaultLocationE(i, location)
}

func ToDurationE(i interface{}) (d time.Duration, err error) {
	return cast.ToDurationE(i)
}

func ToBoolE(i interface{}) (bool, error) {
	return cast.ToBoolE(i)
}

func ToFloat64E(i interface{}) (float64, error) {
	return cast.ToFloat64E(i)
}

func ToFloat32E(i interface{}) (float32, error) {
	return cast.ToFloat32E(i)
}

func ToInt64E(i interface{}) (int64, error) {
	return cast.ToInt64E(i)
}

func ToInt32E(i interface{}) (int32, error) {
	return cast.ToInt32E(i)
}

func ToInt16E(i interface{}) (int16, error) {
	return cast.ToInt16E(i)
}

func ToInt8E(i interface{}) (int8, error) {
	return cast.ToInt8E(i)
}

func ToIntE(i interface{}) (int, error) {
	return cast.ToIntE(i)
}

func ToUintE(i interface{}) (uint, error) {
	return cast.ToUintE(i)
}

func ToUint64E(i interface{}) (uint64, error) {
	return cast.ToUint64E(i)
}

func ToUint32E(i interface{}) (uint32, error) {
	return cast.ToUint32E(i)
}

func ToUint16E(i interface{}) (uint16, error) {
	return cast.ToUint16E(i)
}

func ToUint8E(i interface{}) (uint8, error) {
	return cast.ToUint8E(i)
}

func ToStringE(i interface{}) (string, error) {
	return cast.ToStringE(i)
}

func ToStringMapStringE(i interface{}) (map[string]string, error) {
	return cast.ToStringMapStringE(i)
}

func ToStringMapStringSliceE(i interface{}) (map[string][]string, error) {
	return cast.ToStringMapStringSliceE(i)
}

func ToStringMapBoolE(i interface{}) (map[string]bool, error) {
	return cast.ToStringMapBoolE(i)
}

func ToStringMapE(i interface{}) (map[string]interface{}, error) {
	return cast.ToStringMapE(i)
}

func ToStringMapIntE(i interface{}) (map[string]int, error) {
	return cast.ToStringMapIntE(i)
}

func ToStringMapInt64E(i interface{}) (map[string]int64, error) {
	return cast.ToStringMapInt64E(i)
}

func ToSliceE(i interface{}) ([]interface{}, error) {
	return cast.ToSliceE(i)
}

func ToBoolSliceE(i interface{}) ([]bool, error) {
	return cast.ToBoolSliceE(i)
}

func ToStringSliceE(i interface{}) ([]string, error) {
	return cast.ToStringSliceE(i)
}

func ToIntSliceE(i interface{}) ([]int, error) {
	return cast.ToIntSliceE(i)
}

func ToDurationSliceE(i interface{}) ([]time.Duration, error) {
	return cast.ToDurationSliceE(i)
}

func ToBool(i interface{}) bool {
	v, _ := ToBoolE(i)
	return v
}

func ToTime(i interface{}) time.Time {
	v, _ := ToTimeE(i)
	return v
}

func ToTimeInDefaultLocation(i interface{}, location *time.Location) time.Time {
	v, _ := ToTimeInDefaultLocationE(i, location)
	return v
}

func ToDuration(i interface{}) time.Duration {
	v, _ := ToDurationE(i)
	return v
}

func ToFloat64(i interface{}) float64 {
	v, _ := ToFloat64E(i)
	return v
}

func ToFloat32(i interface{}) float32 {
	v, _ := ToFloat32E(i)
	return v
}

func ToInt64(i interface{}) int64 {
	v, _ := ToInt64E(i)
	return v
}

func ToInt32(i interface{}) int32 {
	v, _ := ToInt32E(i)
	return v
}

func ToInt16(i interface{}) int16 {
	v, _ := ToInt16E(i)
	return v
}

func ToInt8(i interface{}) int8 {
	v, _ := ToInt8E(i)
	return v
}

func ToInt(i interface{}) int {
	v, _ := ToIntE(i)
	return v
}

func ToUint(i interface{}) uint {
	v, _ := ToUintE(i)
	return v
}

func ToUint64(i interface{}) uint64 {
	v, _ := ToUint64E(i)
	return v
}

func ToUint32(i interface{}) uint32 {
	v, _ := ToUint32E(i)
	return v
}

func ToUint16(i interface{}) uint16 {
	v, _ := ToUint16E(i)
	return v
}

func ToUint8(i interface{}) uint8 {
	v, _ := ToUint8E(i)
	return v
}

func ToString(i interface{}) string {
	v, _ := ToStringE(i)
	return v
}

func ToStringMapString(i interface{}) map[string]string {
	v, _ := ToStringMapStringE(i)
	return v
}

func ToStringMapStringSlice(i interface{}) map[string][]string {
	v, _ := ToStringMapStringSliceE(i)
	return v
}

func ToStringMapBool(i interface{}) map[string]bool {
	v, _ := ToStringMapBoolE(i)
	return v
}

func ToStringMapInt(i interface{}) map[string]int {
	v, _ := ToStringMapIntE(i)
	return v
}

func ToStringMapInt64(i interface{}) map[string]int64 {
	v, _ := ToStringMapInt64E(i)
	return v
}

func ToStringMap(i interface{}) map[string]interface{} {
	v, _ := ToStringMapE(i)
	return v
}

func ToSlice(i interface{}) []interface{} {
	v, _ := ToSliceE(i)
	return v
}

func ToBoolSlice(i interface{}) []bool {
	v, _ := ToBoolSliceE(i)
	return v
}

func ToStringSlice(i interface{}) []string {
	v, _ := ToStringSliceE(i)
	return v
}

func ToIntSlice(i interface{}) []int {
	v, _ := ToIntSliceE(i)
	return v
}

func ToDurationSlice(i interface{}) []time.Duration {
	v, _ := ToDurationSliceE(i)
	return v
}