package util

import (
	"github.com/tyler-smith/go-bip32"
	"github.com/tyler-smith/go-bip39"
)

type Account struct {
	Mnemonic string
	Password string
	Seed []byte
	PrivateKey *bip32.Key
	PublicKey *bip32.Key
}

func NewAccount(passwd string) (*Account, error) {
	entropy, _ := bip39.NewEntropy(256)
	mnemonic, _ := bip39.NewMnemonic(entropy)

	return GetAccount(mnemonic, passwd)
}

func GetAccount(mnemonic, passwd string) (*Account, error) {
	seed := bip39.NewSeed(mnemonic, passwd)
	privateKey, err := bip32.NewMasterKey(seed)
	if err != nil {
		return nil, err
	}

	publicKey := privateKey.PublicKey()

	return &Account{
		Mnemonic:   mnemonic,
		Password:   passwd,
		Seed:       seed,
		PrivateKey: privateKey,
		PublicKey:  publicKey,
	}, nil
}