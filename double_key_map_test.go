package util

import (
	"testing"
)

func TestDoubleKeyMap(t *testing.T) {
	t.Run("double_key_map", func(t *testing.T) {
		m := NewDoubleKeyMap()
		m.Set("a", 1, "a1")
		m.Set(2, "b", "2b")
		if m.GetByK1("a") != "a1" || m.GetByK2(1) != "a1" {
			t.Fail()
		}

		if m.GetByK1(1) == "a1" || m.GetByK2("a") == "a1" {
			t.Fail()
		}

		if m.GetByK1(2) != "2b" || m.GetByK2("b") != "2b" {
			t.Fail()
		}

		if m.GetByK1("b") == "2b" || m.GetByK2(2) == "2b" {
			t.Fail()
		}

		m.DelByK2("b")
		if m.GetByK2("b") != nil {
			t.Fail()
		}

		var idx int
		m.Foreach(func(k1, k2, v interface{}) bool {
			switch idx {
			case 0:
				if k1 != "a" || k2 != 1 || v != "a1" {
					t.Fail()
				}
			case 1:
				if k1 != 2 || k2 != "b" || v != "2b" {
					t.Fail()
				}
			}
			idx++
			return true
		})
	})
}
