package util

import (
	"container/list"
)

type DoubleKeyMap struct {
	container *list.List
	key1      map[interface{}]*list.Element
	key2      map[interface{}]*list.Element
	rIndex    map[*list.Element][]interface{}
}

func NewDoubleKeyMap() *DoubleKeyMap {
	return &DoubleKeyMap{
		container: list.New(),
		key1:      make(map[interface{}]*list.Element),
		key2:      make(map[interface{}]*list.Element),
		rIndex:    make(map[*list.Element][]interface{}),
	}
}

func (m *DoubleKeyMap) Set(k1, k2, v interface{}) {
	e := m.container.PushBack(v)
	m.key1[k1] = e
	m.key2[k2] = e
	m.rIndex[e] = []interface{}{k1, k2}
}

func (m *DoubleKeyMap) GetByK1(k1 interface{}) interface{} {
	return m.get(m.key1, k1)
}

func (m *DoubleKeyMap) GetByK2(k1 interface{}) interface{} {
	return m.get(m.key2, k1)
}

func (m *DoubleKeyMap) GetK1(k2 interface{}) interface{} {
	e, ok := m.key2[k2]
	if !ok {
		return nil
	}

	idx, ok := m.rIndex[e]
	if !ok {
		return nil
	}

	return idx[0]
}

func (m *DoubleKeyMap) GetK2(k1 interface{}) interface{} {
	e, ok := m.key1[k1]
	if !ok {
		return nil
	}

	idx, ok := m.rIndex[e]
	if !ok {
		return nil
	}

	return idx[1]
}

func (m *DoubleKeyMap) DelByK1(k1 interface{}) bool {
	e, ok := m.key1[k1]
	if !ok {
		return false
	}

	delete(m.key1, k1)
	m.container.Remove(e)

	idx, ok := m.rIndex[e]
	if !ok {
		return true
	}

	delete(m.rIndex, e)
	delete(m.key2, idx[1])
	return true
}

func (m *DoubleKeyMap) DelByK2(k2 interface{}) bool {
	e, ok := m.key2[k2]
	if !ok {
		return false
	}

	delete(m.key2, k2)
	m.container.Remove(e)

	idx, ok := m.rIndex[e]
	if !ok {
		return true
	}

	delete(m.rIndex, e)
	delete(m.key1, idx[0])
	return true
}

func (m *DoubleKeyMap) get(index map[interface{}]*list.Element, k1 interface{}) interface{} {
	e, ok := index[k1]
	if !ok {
		return nil
	}

	return e.Value
}

func (m *DoubleKeyMap) Foreach(fn func(k1, k2, v interface{}) bool) {
	e := m.container.Front()
	for ; e != nil; e = e.Next() {
		idx := m.rIndex[e]
		if !fn(idx[0], idx[1], e.Value) {
			break
		}
	}
}
